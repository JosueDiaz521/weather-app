package com.example.weatherapp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MainWeatherFragment : Fragment() {

    lateinit var cityName: TextView
    lateinit var weatherName: TextView
    lateinit var searchButton: Button
    lateinit var weatherImage: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cityName = view.findViewById(R.id.city_name)
        weatherName = view.findViewById(R.id.weather_response)
        searchButton = view.findViewById(R.id.search_button)
        weatherImage = view.findViewById(R.id.weather_image)

        searchButton.setOnClickListener{
            getWeather()
        }
    }

    fun getWeather(){
        val weatherService = WeatherService.instance
        var weatherCall = weatherService.getWeather("2.5",cityName.text.toString())
        //enqueque es asyncrona 99.9% se usa para no bloquear el hilo principal para no bloquear la pantalla al usuario
        weatherCall.enqueue(object: Callback<WeatherResponse> {
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {

            }

            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                val weatherList = response.body() as WeatherResponse
                val firstWeather = weatherList.weather?.get(0)

                weatherName.text = firstWeather?.name?: "Error"

                val iconUrl =
                        "https://api.openweathermap.org/img/w/${firstWeather?.icon ?: "10d"}.png"
                Glide.with(context!!)
                    .load(iconUrl)
                    .into(weatherImage)
            }

        })
    }

}
