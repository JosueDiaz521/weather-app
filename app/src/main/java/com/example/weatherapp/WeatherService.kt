package com.example.weatherapp

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
//Get es para retornar
//Post es para crear
//Put es para actualizar un elemento
//Patch es para actualizar varios elementos


interface WeatherService {
    @GET("/data/{api_version}/weather")
    fun getWeather(
        @Path ("api_version") version:String = "2.5",
        @Query("q") city:String,
        @Query("appid") appId:String = "2e89d53487c9c12f762cba59cbf3eda0"

    ) : Call<WeatherResponse>

    companion object {
        val instance: WeatherService by lazy{
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }

}

