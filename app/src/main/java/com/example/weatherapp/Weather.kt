package com.example.weatherapp

import com.google.gson.annotations.SerializedName

data class WeatherResponse (var weather: List<Weather>?){}

data class Weather (
    @SerializedName("main")
    var name:String?,
    var description: String?,
    var icon:String?){}